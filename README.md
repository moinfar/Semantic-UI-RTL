# Semantic-UI-RTL

RTL Build of Semantic UI.
This repo is just a simple config to build the official RTL version of semantic-ui.


# How to Use

## Build One for Yourself!

Run build stage in the project pipeline at GtiLab and enjoy!

## Use npm

you can install the latest build using:

```
npm install semantic-ui-rtl
```
